import * as crypto from "crypto";
import {Transaction} from "./Transaction";
const eccrypto = require("eccrypto");

export class Transactions {
    private minerRewardTransaction: Transaction;
    private transactions: Transaction[] = [];
    private validTransactions: Transaction[] = [];

    constructor(minerReward: number) {
        this.minerRewardTransaction = new Transaction("network", "", minerReward, 0, "");
    }

    public addTransactions(transaction: Transaction): void {
        this.verifySignatureTransaction(transaction);
    }

    public addMinerRewardAddress(minerAddress: string): void {
        this.minerRewardTransaction.setTo(minerAddress);
    }

    public getTransactions(): Transaction[] {
        this.validTransactions.push(this.minerRewardTransaction);
        this.transactions.forEach( (transaction: Transaction) => {
            this.validTransactions.push(transaction);
        });

        return this.validTransactions;
    }

    protected verifySignatureTransaction(transaction: Transaction) {

        let str = transaction.getFrom()     +
                  transaction.getTo()       +
                  transaction.getAmount()   +
                  transaction.getMinerFee();

        let msg = crypto.createHash("sha256").update(str).digest();

        let publicAddress = new Buffer(transaction.getFrom(), "base64");
        let signature = new Buffer(transaction.getSignature(), "base64");

        eccrypto.verify(publicAddress, msg, signature).then( () => {
            console.log("transaction ok!");
            this.minerRewardTransaction.addMinerFee(transaction.getMinerFee());
            this.transactions.push(transaction);
        });
    }
}