export class Transaction {
    private from: string;
    private to: string;
    private amount: number;
    private minerFee: number = 0;
    private signature: string;

    constructor(from: string, to: string, amount: number, minerFee: number, signature: string) {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.minerFee = minerFee;
        this.signature = signature;
    }

    public getFrom(): string {
        return this.from;
    }

    public getTo(): string {
        return this.to;
    }

    public getAmount(): number {
        return this.amount;
    }

    public getMinerFee(): number {
        return this.minerFee;
    }

    public getSignature(): string {
        return this.signature;
    }

    public addMinerFee(minerFee: number) {
        this.minerFee += minerFee;
    }

    public setTo(to: string) {
        this.to = to;
    }
}