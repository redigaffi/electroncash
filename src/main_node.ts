#!/usr/bin/env node
import { Block } from "./Block/Block";
import { BlockChain } from "./BlockChain/BlockChain";
import { Data } from "./Block/Data";
import { Transaction } from "./Transaction/Transaction";
import { ProofOfWork } from "./ProofOfWork/ProofOfWork";
import {Transactions} from "./Transaction/Transactions";


const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let blockChain: BlockChain = new BlockChain();
let transactions: Transactions = new Transactions(blockChain.getBlockReward());

app.post("/txion", (req: any, res: any) => {
        let tx = req.body;
        transactions.addTransactions(new Transaction(tx.from,
                                                     tx.to, Number(tx.amount),
                                                     Number(tx.minerFee),
                                                     String(tx.signature)));

    }
);

app.get("/mine", (req: any, res: any) => {
        transactions.addMinerRewardAddress("aaaa-aaaa-aaaa-aaaas");

        let lastBlock = blockChain.getLastBlock();

        let newBlock: Block = new Block(
                lastBlock.getIndex() + 1,
                new Data(transactions.getTransactions()),
                lastBlock.getHash(),
                blockChain.getDifficulty()
        );

        let proofOfNewBlock = new ProofOfWork().proofOfWork(newBlock.getChallenge(), blockChain.getDifficulty());
        newBlock.setProofOfWork(proofOfNewBlock);

        console.log(newBlock);

        blockChain.addBlock(newBlock);
        transactions = new Transactions(blockChain.getBlockReward());
    }
);

app.listen(3000);
