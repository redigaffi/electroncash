import {Transaction} from "../Transaction/Transaction";

export class Data {
    private transactions: Transaction[] = [];

    constructor(transactions: Transaction[]) {
        this.transactions = transactions;
    }

    public getTransactions(): Transaction[] {
        return this.transactions;
    }
}
