import {stringify} from "querystring";
import {Data} from "./Data";
const sha256 = require("sha256");

export class Block {
    private index: number;
    private timestamp: number;
    private data: Data;
    private proofOfWork: any;
    private target: any;
    private previousHash: string;
    private hash: string;
    private challenge: any;

    constructor(index: number, data: Data, previousHash: string, target: any) {
        this.index = index;
        this.timestamp = new Date().getTime();
        this.data = data;
        this.previousHash = previousHash;
        this.target = target;
        this.challenge = sha256( this.index +
                                 this.timestamp +
                                 JSON.stringify(this.data) +
                                 this.previousHash  +
                                 this.target);
    }

    public hashBlock() {
        this.hash = sha256(
            this.index +
            stringify(this.timestamp) +
            JSON.stringify(this.data) +
            this.challenge +
            this.proofOfWork +
            this.target +
            this.previousHash);
    }

    public getIndex() {
        return this.index;
    }

    public getTimestamp() {
        return this.timestamp;
    }

    public getData(): Data {
        return this.data;
    }

    public getHash() {
        return this.hash;
    }

    public getProofOfWork() {
        return this.proofOfWork;
    }

    public getTarget() {
        return this.target;
    }

    public setProofOfWork(proofOfWork: any) {
        this.proofOfWork = proofOfWork;
    }

    public setTimeStamp(timeStamp: any) {
        this.timestamp = timeStamp;
    }

    public getChallenge(): any {
        return this.challenge;
    }
}
