import {Block} from "../Block/Block";
import {Data} from "../Block/Data";
const BigInt = require("big-integer");

export class BlockChain {
    private blockChain: Block[] = [];
    private difficulty: any  = new BigInt("757540259738344171072190464662720734373784222848779065093617340121088000");
    private blockReward: number = 10;

    constructor() {
        this.generateGenesisBlock();
    }

    public addBlock(block: Block): void {
        block.setTimeStamp(new Date().getTime());
        block.hashBlock();
        this.blockChain.push(block);
        this.adjustDifficulty();
    }

    public getLastBlock(): Block {
        return this.blockChain[this.blockChain.length - 1];
    }

    public generateGenesisBlock(): void {
        let genesisBlock = new Block(0, new Data([]), "0", this.difficulty);
        this.addBlock(genesisBlock);
    }

    public adjustDifficulty(): void {
        let amountOfBlocks: number = this.blockChain.length;

        if (amountOfBlocks % 2 === 0) {
            console.log("Adjusting difficulty.")
            console.log("Actual: " + this.difficulty.toString());

            let lastBlock: Block = this.getLastBlock();
            let blockBefore: Block = this.blockChain[amountOfBlocks - 2];

            let lastBlockTime: any = new Date(lastBlock.getTimestamp() );
            let blockBeforeTime: any = new Date(blockBefore.getTimestamp());

            console.log(lastBlockTime.getTime());
            console.log(blockBeforeTime.getTime());

            let diffTimeSeconds: number = (lastBlockTime.getTime() - blockBeforeTime.getTime()) / 1000;
            let goodRatio = 2 / 120;
            let actualRatio = 2 / diffTimeSeconds;

            console.log(goodRatio);
            console.log(diffTimeSeconds);
            console.log(actualRatio);

            let differenceRatio = actualRatio - goodRatio;
            console.log(differenceRatio);

            // difficult too easy
            if (differenceRatio > 0) {
                console.log("new diff");
                this.difficulty =  this.difficulty.multiply(Math.round(diffTimeSeconds)).divide(950);
                console.log(this.difficulty.toString());
            // Difficulty to hard
            } else if (differenceRatio < 0) {
                this.difficulty =  this.difficulty.multiply(Math.round(diffTimeSeconds)).divide(950);
                console.log("diff hard");
            // Difficulty ok
            } else if (differenceRatio === 0 ) {

                console.log("diff ok");
            }


        }
    }

    public getDifficulty(): number {
        return this.difficulty;
    }

    public getBlockReward(): number {
        return this.blockReward;
    }

}
