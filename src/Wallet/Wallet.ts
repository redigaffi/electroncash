import {Transaction} from "../Transaction/Transaction";

export class Wallet {
    private publicAddress: string;
    private amountAvailable: number;
    private transactions: Transaction[];

    constructor(publicAddress: string, amountAvailable: number, transactions: Transaction[]) {
        this.publicAddress = publicAddress;
        this.amountAvailable = amountAvailable;
        this.transactions = transactions;
    }
}
