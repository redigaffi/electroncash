import {MongoClient} from "mongodb";

export class MainRepository {
    protected db: any;
    private url: string = "mongodb://localhost:27017/coin";

    constructor() {
        this.startConnection();
    }

    private startConnection() {
        this.db = new Promise((resolve: any, reject: any) => {

            MongoClient.connect(this.url, (err: any, db: any) => {
                if (!err) {
                    resolve(db);
                } else {
                    reject();
                }
            });
        });

    }
}
