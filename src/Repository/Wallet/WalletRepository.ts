import {Db} from "mongodb";
import {Wallet} from "../../Wallet/Wallet";
import {MainRepository} from "../MainRepository";

export class WalletRepository extends MainRepository {

    private static COLLECTION_NAME = "wallet";

    constructor() {
        super();
    }

    public findByPublicAddress(walletAddress: string): Promise<Wallet> {

        return this.db.then( (db: Db) => {

            let wallet = db.collection(WalletRepository.COLLECTION_NAME)
                           .findOne({publicAddress: walletAddress});

            return wallet.then((value: any) => {
                return new Wallet(value.publicAddress, value.amountAvailable, []);
            });

        });
    }
}
