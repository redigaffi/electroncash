const sha256 = require("sha256");
const BigInt = require("big-integer");

export class ProofOfWork {

    public proofOfWork(challenge: string, difficulty: number): number {

        let count = 0;

        while (!this.validateWork(challenge, count, difficulty)) {
            count++;
        }

        return count;
    }

    public validateWork(challenge: any, response: any, target: any): boolean {
        let result = new BigInt(sha256(challenge + response), 16);
        return result.lt(target);
    }

}
