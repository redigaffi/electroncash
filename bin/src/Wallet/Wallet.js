"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Wallet = /** @class */ (function () {
    function Wallet(publicAddress, amountAvailable, transactions) {
        this.publicAddress = publicAddress;
        this.amountAvailable = amountAvailable;
        this.transactions = transactions;
    }
    return Wallet;
}());
exports.Wallet = Wallet;
