"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Block_1 = require("../Block/Block");
var Data_1 = require("../Block/Data");
var BlockChain = /** @class */ (function () {
    function BlockChain() {
        this.blockChain = [];
        this.difficulty = 6;
        this.blockReward = 10;
        this.generateGenesisBlock();
    }
    BlockChain.prototype.addBlock = function (block) {
        this.blockChain.push(block);
        this.adjustDifficulty();
    };
    BlockChain.prototype.getLastBlock = function () {
        return this.blockChain[this.blockChain.length - 1];
    };
    BlockChain.prototype.generateGenesisBlock = function () {
        var genesisBlock = new Block_1.Block(0, new Data_1.Data([]), "0", 9, this.difficulty);
        this.addBlock(genesisBlock);
    };
    /**
     * Every 10 blocks,  increment difficulty and reduce reward
     */
    BlockChain.prototype.adjustDifficulty = function () {
        if (this.blockChain.length % 10 === 0) {
            this.difficulty++;
            this.blockReward -= 1;
        }
    };
    BlockChain.prototype.getDifficulty = function () {
        return this.difficulty;
    };
    BlockChain.prototype.getBlockReward = function () {
        return this.blockReward;
    };
    return BlockChain;
}());
exports.BlockChain = BlockChain;
