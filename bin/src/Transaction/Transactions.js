"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crypto = require("crypto");
var Transaction_1 = require("./Transaction");
var eccrypto = require("eccrypto");
var Transactions = /** @class */ (function () {
    function Transactions(minerReward) {
        this.transactions = [];
        this.validTransactions = [];
        this.minerRewardTransaction = new Transaction_1.Transaction("network", "", minerReward, 0, "");
    }
    Transactions.prototype.addTransactions = function (transaction) {
        this.verifySignatureTransaction(transaction);
    };
    Transactions.prototype.addMinerRewardAddress = function (minerAddress) {
        this.minerRewardTransaction.setTo(minerAddress);
    };
    Transactions.prototype.getTransactions = function () {
        var _this = this;
        this.validTransactions.push(this.minerRewardTransaction);
        this.transactions.forEach(function (transaction) {
            _this.validTransactions.push(transaction);
        });
        return this.validTransactions;
    };
    Transactions.prototype.verifySignatureTransaction = function (transaction) {
        var _this = this;
        var str = transaction.getFrom() +
            transaction.getTo() +
            transaction.getAmount() +
            transaction.getMinerFee();
        var msg = crypto.createHash("sha256").update(str).digest();
        var publicAddress = new Buffer(transaction.getFrom(), "base64");
        var signature = new Buffer(transaction.getSignature(), "base64");
        eccrypto.verify(publicAddress, msg, signature).then(function () {
            _this.minerRewardTransaction.addMinerFee(transaction.getMinerFee());
            _this.transactions.push(transaction);
        });
    };
    return Transactions;
}());
exports.Transactions = Transactions;
