"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Transaction = /** @class */ (function () {
    function Transaction(from, to, amount, minerFee, signature) {
        this.minerFee = 0;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.minerFee = minerFee;
        this.signature = signature;
    }
    Transaction.prototype.getFrom = function () {
        return this.from;
    };
    Transaction.prototype.getTo = function () {
        return this.to;
    };
    Transaction.prototype.getAmount = function () {
        return this.amount;
    };
    Transaction.prototype.getMinerFee = function () {
        return this.minerFee;
    };
    Transaction.prototype.getSignature = function () {
        return this.signature;
    };
    Transaction.prototype.addMinerFee = function (minerFee) {
        this.minerFee += minerFee;
    };
    Transaction.prototype.setTo = function (to) {
        this.to = to;
    };
    return Transaction;
}());
exports.Transaction = Transaction;
