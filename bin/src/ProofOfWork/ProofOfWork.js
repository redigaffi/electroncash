"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sha256 = require("sha256");
var ProofOfWork = /** @class */ (function () {
    function ProofOfWork() {
    }
    ProofOfWork.prototype.proofOfWork = function (lastBlock, difficulty) {
        var increment = lastBlock.getProofOfWork() + 1;
        while (!this.validateWork(lastBlock, increment, difficulty)) {
            increment++;
        }
        return increment;
    };
    ProofOfWork.prototype.validateWork = function (lastBlock, proof, difficulty) {
        var proofString = sha256(JSON.stringify(lastBlock) + String(proof));
        if (proofString.slice(-difficulty) === this.repeatStringToFind(difficulty)) {
            return true;
        }
        return false;
    };
    ProofOfWork.prototype.repeatStringToFind = function (amount) {
        var tmpString = "";
        for (var i = 0; i < amount; i++) {
            tmpString += "0";
        }
        return tmpString;
    };
    return ProofOfWork;
}());
exports.ProofOfWork = ProofOfWork;
