"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var querystring_1 = require("querystring");
var sha256 = require("sha256");
var Block = /** @class */ (function () {
    function Block(index, data, previousHash, proofOfWork, difficulty) {
        this.index = index;
        this.timestamp = new Date().getTime();
        this.data = data;
        this.previousHash = previousHash;
        this.hash = this.hashBlock();
        this.proofOfWork = proofOfWork;
        this.difficulty = difficulty;
    }
    Block.prototype.hashBlock = function () {
        return sha256(this.index +
            querystring_1.stringify(this.timestamp) +
            JSON.stringify(this.data) +
            this.proofOfWork +
            this.difficulty +
            this.previousHash);
    };
    Block.prototype.getIndex = function () {
        return this.index;
    };
    Block.prototype.getTimestamp = function () {
        return this.timestamp;
    };
    Block.prototype.getData = function () {
        return this.data;
    };
    Block.prototype.getHash = function () {
        return this.hash;
    };
    Block.prototype.getProofOfWork = function () {
        return this.proofOfWork;
    };
    Block.prototype.getDifficulty = function () {
        return this.difficulty;
    };
    return Block;
}());
exports.Block = Block;
