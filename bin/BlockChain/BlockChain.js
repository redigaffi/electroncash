"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Block_1 = require("../Block/Block");
var Data_1 = require("../Block/Data");
var BigInt = require("big-integer");
var BlockChain = /** @class */ (function () {
    function BlockChain() {
        this.blockChain = [];
        this.difficulty = new BigInt("757540259738344171072190464662720734373784222848779065093617340121088000");
        this.blockReward = 10;
        this.generateGenesisBlock();
    }
    BlockChain.prototype.addBlock = function (block) {
        block.setTimeStamp(new Date().getTime());
        block.hashBlock();
        this.blockChain.push(block);
        this.adjustDifficulty();
    };
    BlockChain.prototype.getLastBlock = function () {
        return this.blockChain[this.blockChain.length - 1];
    };
    BlockChain.prototype.generateGenesisBlock = function () {
        var genesisBlock = new Block_1.Block(0, new Data_1.Data([]), "0", this.difficulty);
        this.addBlock(genesisBlock);
    };
    BlockChain.prototype.adjustDifficulty = function () {
        var amountOfBlocks = this.blockChain.length;
        if (amountOfBlocks % 2 === 0) {
            console.log("Adjusting difficulty.");
            console.log("Actual: " + this.difficulty.toString());
            var lastBlock = this.getLastBlock();
            var blockBefore = this.blockChain[amountOfBlocks - 2];
            var lastBlockTime = new Date(lastBlock.getTimestamp());
            var blockBeforeTime = new Date(blockBefore.getTimestamp());
            console.log(lastBlockTime.getTime());
            console.log(blockBeforeTime.getTime());
            var diffTimeSeconds = (lastBlockTime.getTime() - blockBeforeTime.getTime()) / 1000;
            var goodRatio = 2 / 120;
            var actualRatio = 2 / diffTimeSeconds;
            console.log(goodRatio);
            console.log(diffTimeSeconds);
            console.log(actualRatio);
            var differenceRatio = actualRatio - goodRatio;
            console.log(differenceRatio);
            // difficult too easy
            if (differenceRatio > 0) {
                console.log("new diff");
                this.difficulty = this.difficulty.multiply(Math.round(diffTimeSeconds)).divide(950);
                console.log(this.difficulty.toString());
                // Difficulty to hard
            }
            else if (differenceRatio < 0) {
                this.difficulty = this.difficulty.multiply(Math.round(diffTimeSeconds)).divide(950);
                console.log("diff hard");
                // Difficulty ok
            }
            else if (differenceRatio === 0) {
                console.log("diff ok");
            }
        }
    };
    BlockChain.prototype.getDifficulty = function () {
        return this.difficulty;
    };
    BlockChain.prototype.getBlockReward = function () {
        return this.blockReward;
    };
    return BlockChain;
}());
exports.BlockChain = BlockChain;
