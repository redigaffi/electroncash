"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sha256 = require("sha256");
var BigInt = require("big-integer");
var ProofOfWork = /** @class */ (function () {
    function ProofOfWork() {
    }
    ProofOfWork.prototype.proofOfWork = function (challenge, difficulty) {
        var count = 0;
        while (!this.validateWork(challenge, count, difficulty)) {
            count++;
        }
        return count;
    };
    ProofOfWork.prototype.validateWork = function (challenge, response, target) {
        var result = new BigInt(sha256(challenge + response), 16);
        return result.lt(target);
    };
    return ProofOfWork;
}());
exports.ProofOfWork = ProofOfWork;
