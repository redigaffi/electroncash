"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Wallet_1 = require("../../Wallet/Wallet");
var MainRepository_1 = require("../MainRepository");
var WalletRepository = /** @class */ (function (_super) {
    __extends(WalletRepository, _super);
    function WalletRepository() {
        return _super.call(this) || this;
    }
    WalletRepository.prototype.findByPublicAddress = function (walletAddress) {
        return this.db.then(function (db) {
            var wallet = db.collection(WalletRepository.COLLECTION_NAME)
                .findOne({ publicAddress: walletAddress });
            return wallet.then(function (value) {
                return new Wallet_1.Wallet(value.publicAddress, value.amountAvailable, []);
            });
        });
    };
    WalletRepository.COLLECTION_NAME = "wallet";
    return WalletRepository;
}(MainRepository_1.MainRepository));
exports.WalletRepository = WalletRepository;
