"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongodb_1 = require("mongodb");
var MainRepository = /** @class */ (function () {
    function MainRepository() {
        this.url = "mongodb://localhost:27017/coin";
        this.startConnection();
    }
    MainRepository.prototype.startConnection = function () {
        var _this = this;
        this.db = new Promise(function (resolve, reject) {
            mongodb_1.MongoClient.connect(_this.url, function (err, db) {
                if (!err) {
                    resolve(db);
                }
                else {
                    reject();
                }
            });
        });
    };
    return MainRepository;
}());
exports.MainRepository = MainRepository;
