"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Transactions = /** @class */ (function () {
    function Transactions(transactions, proofOfWork) {
        this.transactions = [];
        this.transactions = transactions;
        this.proofOfWork = proofOfWork;
    }
    Transactions.prototype.getTransactions = function () {
        return this.transactions;
    };
    Transactions.prototype.getProofOfWork = function () {
        return this.proofOfWork;
    };
    return Transactions;
}());
exports.Transactions = Transactions;
