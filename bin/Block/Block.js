"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var querystring_1 = require("querystring");
var sha256 = require("sha256");
var Block = /** @class */ (function () {
    function Block(index, data, previousHash, target) {
        this.index = index;
        this.timestamp = new Date().getTime();
        this.data = data;
        this.previousHash = previousHash;
        this.target = target;
        this.challenge = sha256(this.index +
            this.timestamp +
            JSON.stringify(this.data) +
            this.previousHash +
            this.target);
    }
    Block.prototype.hashBlock = function () {
        this.hash = sha256(this.index +
            querystring_1.stringify(this.timestamp) +
            JSON.stringify(this.data) +
            this.challenge +
            this.proofOfWork +
            this.target +
            this.previousHash);
    };
    Block.prototype.getIndex = function () {
        return this.index;
    };
    Block.prototype.getTimestamp = function () {
        return this.timestamp;
    };
    Block.prototype.getData = function () {
        return this.data;
    };
    Block.prototype.getHash = function () {
        return this.hash;
    };
    Block.prototype.getProofOfWork = function () {
        return this.proofOfWork;
    };
    Block.prototype.getTarget = function () {
        return this.target;
    };
    Block.prototype.setProofOfWork = function (proofOfWork) {
        this.proofOfWork = proofOfWork;
    };
    Block.prototype.setTimeStamp = function (timeStamp) {
        this.timestamp = timeStamp;
    };
    Block.prototype.getChallenge = function () {
        return this.challenge;
    };
    return Block;
}());
exports.Block = Block;
