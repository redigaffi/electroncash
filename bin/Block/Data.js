"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Data = /** @class */ (function () {
    function Data(transactions) {
        this.transactions = [];
        this.transactions = transactions;
    }
    Data.prototype.getTransactions = function () {
        return this.transactions;
    };
    return Data;
}());
exports.Data = Data;
