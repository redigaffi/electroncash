#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Block_1 = require("./Block/Block");
var BlockChain_1 = require("./BlockChain/BlockChain");
var Data_1 = require("./Block/Data");
var Transaction_1 = require("./Transaction/Transaction");
var ProofOfWork_1 = require("./ProofOfWork/ProofOfWork");
var Transactions_1 = require("./Transaction/Transactions");
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var blockChain = new BlockChain_1.BlockChain();
var transactions = new Transactions_1.Transactions(blockChain.getBlockReward());
app.post("/txion", function (req, res) {
    var tx = req.body;
    transactions.addTransactions(new Transaction_1.Transaction(tx.from, tx.to, Number(tx.amount), Number(tx.minerFee), String(tx.signature)));
});
app.get("/mine", function (req, res) {
    transactions.addMinerRewardAddress("aaaa-aaaa-aaaa-aaaas");
    var lastBlock = blockChain.getLastBlock();
    var newBlock = new Block_1.Block(lastBlock.getIndex() + 1, new Data_1.Data(transactions.getTransactions()), lastBlock.getHash(), blockChain.getDifficulty());
    var proofOfNewBlock = new ProofOfWork_1.ProofOfWork().proofOfWork(newBlock.getChallenge(), blockChain.getDifficulty());
    newBlock.setProofOfWork(proofOfNewBlock);
    console.log(newBlock);
    blockChain.addBlock(newBlock);
    transactions = new Transactions_1.Transactions(blockChain.getBlockReward());
});
app.listen(3000);
